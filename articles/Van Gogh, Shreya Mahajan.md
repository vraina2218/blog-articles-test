**<h1>VAN GOGH:**</h1>

*Author Shreya Mahajan*

Vincent van Gogh (1853-1890) was a Dutch post-impressionist painter whose distinctive style and emotionally charged works have left an enduring impact on the world of art. His work is characterized by bold colors, dynamic brushstrokes, and a unique approach to depicting light and form. He used thick layers of paint, creating a textured surface making it much more interesting.


**<h3>His Works:</h3>**

1. *THE STARRY NIGHT*
<img src="![starry night](<starry night.webp>)" width="300" height="400">

2. *SUNFLOWERS*
<img src="![sunflowers](sunflowers.webp)" width="300" height="400">

3. *THE BIOGRAPHY: VAN GOGH*
<img src="![van gogh](<van gogh-1.webp>)" width="300" height="400">


**<h3>INTERESTING PAINTINGS OF VAN GOGH:</h3>**


-**Self-Portrait or Portrait of Theo Van Gogh:**

The painting was thought to be a self-portrait of Vincent, however, some believe (including me) that it shows his brother Theo. It may be due to the amount of love vincent had for his brother that he could imagine them as one.



-**The Eccentric Nurse:**
Van Gogh , when in the hospital, painted his nurse. The painting however, was a depiction of his crazy mental state. When the nurse was shown the painting, he was mad at how he was depicted with crazy green hair and bold orange coloured skin. The painting was used as a door for the hen hutch, but later sold for millions.




**<h3>Death of Van Gogh:</h3>**

On the one hand, Van gogh’s painting had become a boundless, energetic frenzy of absolute creativity the likes of which the art world rarely sees. On the other, his mental state was on a knife's edge, caught between the visceral thrill of his creative expression and the darkness that crowded in on all sides.

He had hoped that the quiet natural beauty of the French village might be conducive to creativity. For a while, he began to feel better. After a short visit to his brother in Paris in July, he learned that Theo was considering a business opportunity that would mean he would have to quit the art dealership he had managed for many years. Theo's worry over an uncertain financial future infected his brother Vincent who, over the past 10 years, had relied heavily on Theo's financial and emotional support.

In one of his last letters to Theo he wrote, "I feel - a failure. I feel that that's the fate I am accepting and which won't change any more."

On the evening of July 7, 1890, he walked into a wheat field where he had been painting and shot himself in the chest.

In a letter to his wife Johanna, Theo wrote, "One of his last words was, 'I wish I could pass away like this,' and his wish was fulfilled. A few moments and all was over. He had found the rest he could not find on earth…"





